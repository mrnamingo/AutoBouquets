Menus
----------------
 - Local Area Selection
 generate selected bouquets for your SD/HD local region.

 - Numbering System
 Select number system of offcial receiver being
 SD you will get SD number system, E.G Sky Sports 1 (SD) will be on 402
 If you'd used HD number system, 402 would have been Sky Sports 1 HD

 - Scan Mode
 FTA only:
 Only "scans" FTA channels. Please note. Some channels have wrong data on satellite
 Some channels have been hacked to show FTA to work with other features.
 
 Available HD:
 This is the default option. This will scan in the HD chanenls that can be viewed on 3rd party receivers.
 Non-FTA HD chanenls ahve had their encryption glag chanegd to FTA to work with this feature. 
 
 All Channels:
 This will also scan in the HD channels which you cannot view on 3rd party receivers.

 - Bouquet Channel sort
 HD first (default): The first bouquet will contain local channels then the HD channels. 
 Entertainment bouquet will then start at 101
 
 Large First bouquet: The first bouquet will be the long one containng all channels. 
 Entertainment bouquet will therefore start at 1000+
 
 Sort X: These will customise the bouquet contents. Not the larger first one! Try sort 3. If you don't like it, adapt it.
 AB 28.2 will make bouquets as per your sort file.
 
 - Add Ch. Number to Name
 Adds LCN number in front of the channel names. "BBC One" will become "101 - BBC One" 
 
 - Other Extra Channels
 adds testing and none regional channels to Other bouquet.

 - Use HD First Bouquet
 puts the HD channels starting at position 6 immediately
 after 1-5 (BBC1, BBC2, ITV1, C4, C5 for your region).
 yes = placeholder channels are put in category bouquets.
 no = placeholder channels are put in first bouquet only.

 - Use Internal NIT Scan
 updates the lamedb with latest transponders and services by scanning Network Information Table via reader instead
 of running standard 28.2e engima2 automatic service scan.

 - Hide Blank Placeholders
 yes = allows placeholder skipping on openpli based images. Allows placeholder hiding if supported by engima2 version.
 This does not work on old Blackhole images. Fine on OpenBlackhole.
 no = uses normal services for placeholder channels as non-pli based images do not assign correct positions using pli. 

 - Hide Adult/Gaming
 dont generate the adult or the gaming and dating bouquets.

 - Make all Default Bouquets
 this will force default bouquet categories to be generated. If you wish to remove any bouquets from future generations,
 delete the bouquets you don't want and disable this option.

 - Channel Swap
 this allows you to change the generated channel ordering. Edit the example custom_swap.txt file in AutoBouquets folder
 by entering the channel numbers you wish to swap with each other ie. 101-117, you can swap with placeholder channels.
 The numbers are based on England's numbers. As teh four nations ahve differnt numbers, you will have to edit them.

 - Scan Free To Air Only
 this will generate services with free to air flag detection.

 - Enable Script Checks
 turn off to disable script error checks on incompatible stb.

 - Bouquet Display Style
 this will change bouquet header and channel numbering styles.

 - Automatic Updates
 this allows you to set a daily, weekly, monthly update timer.

Tips
----------------
if you want 1, 2, 3 button presses for BBC1, BBC2, ITV1.first disable hd first option in the menu, then simply
delete the "28.2E ---- UK Bouquets ---" from your bouquet this will remove the official epg numbering and set your
Entertainment bouquet channels to starting with 1, 2, 3.

the Plugin does not re-create any bouquets that have been previously removed by the user. if for example you remove
the adult bouquet, it will remain removed the next time you run the plugin and so remain child friendly updatable.
you will need to turn the default bouquets option to "off" you can set default option to create first bouquets again.

changing the order of the bouquets also remains static and does not get re-ordered on subsequent runs. this will have
to be done while not using the HD Bouquet settings option, or your offical numbering with not be correct and match up.

when using custom_swap.txt file for personal channel order, enter the two channels you wish to switch with each other.

101=115
102=142
103=178
104=230
105=171

Prerequisites
----------------
frontend GUI should work on systems using Enigma2, backend script should work on most linux systems.
script requires some common busybox functions, but all these should be standard in your linux image.

Many, many, many thanks to PaphosAl for his perserverance. 

------------------------------------------------------

 Thanks: general helping with feedback and testing...
 ViX Team members, Abu Baniaz, Lincsat, anyone not mentioned.
 
 www.ukcvs.net thanks you for using AutoBouquets E2
 and thanks to PaphosAL for plugin icon.  HAVE FUN!

